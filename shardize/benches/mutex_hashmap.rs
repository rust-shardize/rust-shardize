#[macro_use]
extern crate bencher;

use shardize::shardize;

use bencher::Bencher;
use std::collections::HashMap;
use std::sync::Mutex;
use std::thread;

#[shardize(ShardedMutexHashmap)]
trait MyTrait: Default {
    fn get(&self, key: usize) -> String;
    fn set(&self, key: usize, value: String);
}

impl MyTrait for Mutex<HashMap<usize, String>> {
    fn get(&self, key: usize) -> String {
        self.lock().unwrap().get(&key).unwrap().clone()
    }

    fn set(&self, key: usize, value: String) {
        self.lock().unwrap().insert(key, value);
    }
}

fn my_shard_key(key: usize) -> usize {
    key
}

fn write_to_mutex_hashmap(bench: &mut Bencher) {
    time_writes(bench, Box::leak(Box::new(Mutex::new(HashMap::new()))));
}

fn read_from_mutex_hashmap(bench: &mut Bencher) {
    time_reads(bench, Box::leak(Box::new(Mutex::new(HashMap::new()))));
}

fn write_to_sharded_mutex_hashmap(bench: &mut Bencher) {
    time_writes(
        bench,
        Box::leak(Box::new(ShardedMutexHashmap::<
            Mutex<HashMap<usize, String>>,
            10,
        >::new(&my_shard_key))),
    );
}

fn read_from_sharded_mutex_hashmap(bench: &mut Bencher) {
    time_reads(
        bench,
        Box::leak(Box::new(ShardedMutexHashmap::<
            Mutex<HashMap<usize, String>>,
            10,
        >::new(&my_shard_key))),
    );
}

fn time_writes<T: 'static + MyTrait + Send + Sync>(
    bench: &mut Bencher,
    t: &'static T,
) {
    const NUM_THREADS: usize = 10;
    const NUM_WRITES: usize = 1000;

    bench.iter(|| {
        for _i in 0..NUM_THREADS {
            thread::spawn(move || {
                for i in 0..NUM_WRITES {
                    t.set(i, String::from("x"));
                }
            });
        }
    });
}

fn time_reads<T: 'static + MyTrait + Send + Sync>(
    bench: &mut Bencher,
    t: &'static T,
) {
    const NUM_THREADS: usize = 11;
    const NUM_READS: usize = 1000;

    for _ in 0..NUM_THREADS {
        for i in 0..NUM_READS {
            t.set(i, String::from("x"));
        }
    }

    bench.iter(|| {
        for _ in 0..NUM_THREADS {
            thread::spawn(move || {
                for i in 0..NUM_READS {
                    t.get(i);
                }
            });
        }
    });
}

benchmark_group!(
    benches,
    write_to_mutex_hashmap,
    write_to_sharded_mutex_hashmap,
    read_from_mutex_hashmap,
    read_from_sharded_mutex_hashmap,
);
benchmark_main!(benches);
