use proc_macro2::TokenStream;
use shardize_core::{shardize_transform, MacroConfig, TraitDefinition};
use syn::parse_macro_input;

extern crate proc_macro;

#[proc_macro_attribute]
pub fn shardize(
    attr: proc_macro::TokenStream,
    item: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let macro_config = parse_macro_input!(attr as MacroConfig);
    let trait_definition = parse_macro_input!(item as TraitDefinition);

    let output: TokenStream =
        shardize_transform(macro_config, trait_definition)
            .unwrap_or_else(|e| panic!("{}", e));

    proc_macro::TokenStream::from(output)
}
