# TODO

- [x] Prove we can write a macro to make a sharded version of a container
- [x] Remove use of Vec; replace with const generic array
- [x] Use basic perf tests to prove it can help performance
- [x] Publish crate
- [ ] Link to crates.io and docs.rs from README
- [ ] Try out trybuild
- [ ] Check TODOs in the code
- [ ] Do better than `panic` in the Default impl
- [ ] Optional const `new()`
- [ ] Look at const Default if it's in stable
- [ ] Try annotating the key with e.g. #[key] instead of assuming the name
- [ ] Consider making `shard_key` an associated function of the trait, or an
    additional generic parameter on the sharded struct.
    First draft of this decision: probably not - it seems more onorous than
    passing it in at runtime, but it might save a pointer dereference per
    method call (assuming the optimiser can't fix that anyway...)
- [ ] Decent compiler errors
    - [ ] when you don't provide a key
    - [ ] basic typos when trying to use this
- [ ] More proper benchmarks
- [ ] Documentation
- [ ] Research actual uses of this and prove it can be useful
    - [ ] Other locking containers
    - [ ] Lock-free containers
    - [ ] Each shard is a different database
    - [ ] Each shard is a different network connection
- [ ] Handle traits with generic params (or throw a useful error) - e.g.
    `trait MyTrait<K, V>: Default {`
- [ ] Think about whether we can avoid the need to impl our trait for the
    underlying data structure (e.g. `impl MyTrait for HashMap`).  We think
    there's no viable alternative.  Maybe something like this before your
    trait:
    ```
    #[shardizeautoimpl(HashMap<usize, String>)]
    ```
    ... it would wrap existing functions on the container with identical ones
    in the trait, and therefore requires those function signatures to match
    exactly.  Will require useful error messages, which looks difficult.
- [ ] Investigate nostd
- [ ] Investigate using less dynamic memory?
